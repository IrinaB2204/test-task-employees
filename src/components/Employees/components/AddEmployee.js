import React, {useState} from 'react';
import PropTypes from 'prop-types'
import '../employees.css';

const AddEmployee = ({employees, setNewEmployees, isEmployeeAdd, showAddNameFields}) => {
  const [newEmployeeName, setNewEmployeeName] = useState({firstName: "", lastName: ""});
  const [inputError, setInputError] = useState("");

  const changeName = (value, name) => {
    setNewEmployeeName({...newEmployeeName, [name]: value});
  };

  const showInputError = () => {
    return inputError ? (
      <div className={"input-error"}>{inputError}</div>
    ) : null;
  };

  const checkData = () => {
    if (!newEmployeeName.firstName || !newEmployeeName.lastName) {
      return setInputError("eneter first name and last name")
    }
    return saveEmployer();
  };

  const saveEmployer = () => {
    const newEmployer = {
      first_name: newEmployeeName.firstName,
      last_name: newEmployeeName.lastName,
      id: employees[employees.length - 1].id + 1
    };
    setNewEmployees([...employees, newEmployer]);
    resetAddEmployer();
  };

  const resetAddEmployer = () => {
    showAddNameFields(false);
    setInputError("");
    setNewEmployeeName({firstName: "", lastName: ""});
  };

  const showNewEmployeeField = () => {
    return isEmployeeAdd ? (
      <div>

        <div className={"input-container"}>
          <div>Имя:</div>
          <input value={newEmployeeName.firstName} onChange={(e) => changeName(e.target.value, 'firstName')}/>
          <div>Фамилия:</div>
          <input value={newEmployeeName.lastName} onChange={(e) => changeName(e.target.value, 'lastName')}/>
        </div>

        {showInputError()}

        <div className={"buttons"}>
          <div onClick={resetAddEmployer} className={"delete-button button"}>Cancel</div>
          <div onClick={() => checkData()} className={"save-button button"}>Save</div>
        </div>

      </div>
    ) : null;
  };

  return (
    <React.Fragment>
      {showNewEmployeeField()}
    </React.Fragment>
  )
};

AddEmployee.propTypes = {
  employees: PropTypes.arrayOf(PropTypes.shape({
    first_name: PropTypes.string,
    last_name: PropTypes.string,
    id: PropTypes.number
  })).isRequired,
  setNewEmployees: PropTypes.func.isRequired,
  isEmployeeAdd: PropTypes.bool.isRequired,
  showAddNameFields: PropTypes.func.isRequired
};

export default AddEmployee;
