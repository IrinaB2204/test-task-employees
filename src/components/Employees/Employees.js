import React, {useEffect, useState} from 'react';
import axios from 'axios';
import AddEmployee from "./components/AddEmployee";
import './employees.css';

const Employees = () => {
  const [isEmployeeAdd, setIsEmployeeAdd] = useState(false);
  const [employees, setEmployees] = useState([]);

  const loadEmployees = async () => {
    return await axios("https://reqres.in/api/users?per_page=12");
  };

  useEffect(async () => {
    await loadEmployees()
      .then(result => setEmployees(result.data.data))
      .catch(e => console.log("error:", e));
  }, []);

  const deleteItem = (id) => {
    const newEmployeesList = employees.filter(item => item.id !== id);
    setEmployees(newEmployeesList);
  };

  const employeesView = () => {
    return employees.map(el =>
      <div key={el.id} className={"item-container"}>
        <div className={"item-name"}>{el.first_name} {el.last_name}</div>
        <div className={"delete-button button"} onClick={() => deleteItem(el.id)}>Delete</div>
      </div>
    );
  };

  const showAddNameFields = (val) => {
    setIsEmployeeAdd(val);
  };

  return (
    <div className={"main-container"}>
      <div className={"header"}>Сотрудники</div>
      <div className={"employeers-container"}>
        {employeesView()}
      </div>
      <div className={"save-button button"} onClick={() => showAddNameFields(true)}>Add Employee</div>
      <AddEmployee
        employees={employees}
        setNewEmployees={setEmployees}
        isEmployeeAdd={isEmployeeAdd}
        showAddNameFields={showAddNameFields}
      />
    </div>
  )
};

export default Employees;