import React from 'react';
import {NavLink} from 'react-router-dom';
import './header.css';

class Header extends React.Component {

  render() {

    return (
      <div>
        <header>
          <ul>
            <NavLink to={'/'} exact activeClassName="ActivePageLink">
              <li>
                Главная
              </li>
            </NavLink>
            <NavLink to={'/employees'} activeClassName="ActivePageLink">
              <li>
                Сотрудники
              </li>
            </NavLink>
          </ul>
        </header>
      </div>
    )
  }
}

export default Header;