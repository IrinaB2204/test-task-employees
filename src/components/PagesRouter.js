import React from 'react';
import {Route, Switch} from 'react-router-dom';

import Main from "./Main/Main";
import Employees from "./Employees/Employees";

const PagesRouter = () => {
  return (
    <div>
      <Switch>
        <Route path="/" exact component={Main}/>
        <Route path="/employees" component={Employees}/>
        <Route component={Main}/>
      </Switch>
    </div>
  );
};

export default PagesRouter;