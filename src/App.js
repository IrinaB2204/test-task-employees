import logo from './logo.svg';
import './App.css';
import Header from "./components/Header/Header";
import PagesRouter from "./components/PagesRouter";

function App() {
  return (
    <div>
      <Header/>
      <PagesRouter/>
    </div>
  );
}

export default App;
